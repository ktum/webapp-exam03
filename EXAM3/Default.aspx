﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="EXAM3.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        #form1 {
            height: 1284px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Label ID="Label5" runat="server" Text="Label"></asp:Label>

        <br />
        <br />
        <div style="height: 584px">

            <asp:Label ID="Label8" runat="server" Text="Election Name:             "></asp:Label>
            <asp:TextBox ID="tb_ElecName" runat="server"></asp:TextBox>
            <br />
            <br />
            <asp:Label ID="Label7" runat="server" Text="Start Date"></asp:Label>
            <asp:Calendar ID="cal_StartDate" runat="server" BackColor="White" BorderColor="#3366CC" BorderWidth="1px" CellPadding="1" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt" ForeColor="#003399" Height="200px" Width="220px">
                <DayHeaderStyle BackColor="#99CCCC" ForeColor="#336666" Height="1px" />
                <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
                <OtherMonthDayStyle ForeColor="#999999" />
                <SelectedDayStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
                <TitleStyle BackColor="#003399" BorderColor="#3366CC" BorderWidth="1px" Font-Bold="True" Font-Size="10pt" ForeColor="#CCCCFF" Height="25px" />
                <TodayDayStyle BackColor="#99CCCC" ForeColor="White" />
                <WeekendDayStyle BackColor="#CCCCFF" />
            </asp:Calendar>
            <br />
            <br />
            <asp:Label ID="Label6" runat="server" Text="End Date"></asp:Label>
            <asp:Calendar ID="cal_EndDate" runat="server" BackColor="White" BorderColor="#3366CC" BorderWidth="1px" CellPadding="1" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt" ForeColor="#003399" Height="200px" Width="220px">
                <DayHeaderStyle BackColor="#99CCCC" ForeColor="#336666" Height="1px" />
                <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
                <OtherMonthDayStyle ForeColor="#999999" />
                <SelectedDayStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
                <TitleStyle BackColor="#003399" BorderColor="#3366CC" BorderWidth="1px" Font-Bold="True" Font-Size="10pt" ForeColor="#CCCCFF" Height="25px" />
                <TodayDayStyle BackColor="#99CCCC" ForeColor="White" />
                <WeekendDayStyle BackColor="#CCCCFF" />
            </asp:Calendar>
            <br />
            <asp:Button ID="btn_CreateElec" runat="server" Text="Create" OnClick="btn_CreateElec_Click" />
            <br />
            <br />
        <asp:Label ID="lbl_CreateElecResult" runat="server" Text="Result: "></asp:Label>
        </div>
        <br />
        <br />
        <br />
        <div>
            <asp:Label ID="Label9" runat="server" Text="Position: "></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="tb_Pos" runat="server"></asp:TextBox>
            <br />
            <asp:Label ID="Label10" runat="server" Text="Max Votes:"></asp:Label>
&nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="tb_MaxV" runat="server"></asp:TextBox>
            <br />
            <asp:GridView ID="gv_Positions" runat="server" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3">
                <FooterStyle BackColor="White" ForeColor="#000066" />
                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                <RowStyle ForeColor="#000066" />
                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                <SortedAscendingHeaderStyle BackColor="#007DBB" />
                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                <SortedDescendingHeaderStyle BackColor="#00547E" />
            </asp:GridView>
            <br />
            <asp:Button ID="btn_AddPos" runat="server" OnClick="btn_AddPos_Click" Text="Add" />
            <br />
            <br />
            <asp:Label ID="lbl_PosResult" runat="server" Text="Result: "></asp:Label>
        </div>
        <br />
        <br />
        <br />
        <div style="height: 191px">
        <asp:Label ID="Label2" runat="server" Text="Voter ID:"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="tb_VoterID" runat="server"></asp:TextBox>
        <br />
&nbsp;<asp:Label ID="Label1" runat="server" Text="First Name:"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="tb_FirstName" runat="server"></asp:TextBox>
        <br />
&nbsp;<asp:Label ID="Label3" runat="server" Text="Middle Name:"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="tb_MiddleName" runat="server"></asp:TextBox>
        <br />
&nbsp;<asp:Label ID="Label4" runat="server" Text="Last Name:"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;
        <asp:TextBox ID="tb_LastName" runat="server"></asp:TextBox>
            <br />
        <br />
        <asp:Button ID="btn_AddVoter" runat="server" OnClick="tb_AddVoter_Click" Text="Add Voter" />
            <br />
        <br />
        <asp:Label ID="lbl_Result" runat="server" Text="Result: "></asp:Label>
        </div>
    </form>
</body>
</html>
