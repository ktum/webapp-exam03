﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;

namespace EXAM3
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void tb_AddVoter_Click(object sender, EventArgs e)
        {

            var varUriString = "http://localhost:54103/Service.svc/AddVoter";
            var varHttpWebRequest = (HttpWebRequest)WebRequest.Create(varUriString);
            varHttpWebRequest.ContentType = "application/json";
            varHttpWebRequest.Method = "POST";

            try
            {
                using (var varStreamWriter = new StreamWriter(varHttpWebRequest.GetRequestStream()))
                {

                    StringBuilder sb = new StringBuilder();

                    sb.AppendLine("{");
                    sb.AppendLine("\"VoterID\":\"" + tb_VoterID.Text + "\"");
                    sb.AppendLine(",");
                    sb.AppendLine("\"FN\":\"" + tb_FirstName.Text + "\"");
                    sb.AppendLine(",");
                    sb.AppendLine("\"MN\":\"" + tb_MiddleName.Text + "\"");
                    sb.AppendLine(",");
                    sb.AppendLine("\"LN\":\"" + tb_LastName.Text + "\"");
                    sb.AppendLine("}");

                    varStreamWriter.Write(sb.ToString());
                    varStreamWriter.Flush();
                    varStreamWriter.Close();
                }

                var varHttpWebResponse = (HttpWebResponse)varHttpWebRequest.GetResponse();
                using (var varStreamReader = new StreamReader(varHttpWebResponse.GetResponseStream()))
                {
                    var varResponse = varStreamReader.ReadToEnd();
                    lbl_Result.Text = varResponse;
                }
            }
            catch (Exception ex)
            {
                lbl_Result.Text = "ERROR: " + ex.Message;
            }
        }

        protected void btn_CreateElec_Click(object sender, EventArgs e)
        {
            var varUriString = "http://localhost:54103/Service.svc/AddElection";
            var varHttpWebRequest = (HttpWebRequest)WebRequest.Create(varUriString);
            varHttpWebRequest.ContentType = "application/json";
            varHttpWebRequest.Method = "POST";
            string conS, conD;
            conS = cal_StartDate.SelectedDate.ToString();
            conD = cal_EndDate.SelectedDate.ToString();
            string wiw = "", w3w = "";
            var charsToRem = new string[] { "{", "}",};
            foreach(var c in charsToRem)
            {
                conS = conS.Replace(c, string.Empty);
                conD = conD.Replace(c, string.Empty);

            }
            foreach(char a in conD)
            {
                wiw = wiw + a;
                if(a==' ')
                {
                    break;
                }
            }
            foreach (char a in conS)
            {
                w3w = w3w + a;
                if (a == ' ')
                {
                    break;
                }
            }
            wiw = wiw.Replace('/', 'a');
            w3w = w3w.Replace('/', 'a');
            try
            {
                using (var varStreamWriter = new StreamWriter(varHttpWebRequest.GetRequestStream()))
                {

                    StringBuilder sb = new StringBuilder();

                    sb.AppendLine("{");
                    sb.AppendLine("\"election_name\":\"" + tb_ElecName.Text + "\"");
                    sb.AppendLine(",");
                    sb.AppendLine("\"startD\":\"" + wiw + "\"");
                    sb.AppendLine(",");
                    sb.AppendLine("\"endD\":\"" + w3w + "\"");
                    sb.AppendLine("}");

                    varStreamWriter.Write(sb.ToString());
                    varStreamWriter.Flush();
                    varStreamWriter.Close();
                }

                var varHttpWebResponse = (HttpWebResponse)varHttpWebRequest.GetResponse();
                using (var varStreamReader = new StreamReader(varHttpWebResponse.GetResponseStream()))
                {
                    var varResponse = varStreamReader.ReadToEnd();
                    lbl_CreateElecResult.Text = varResponse;
                }
            }
            catch (Exception ex)
            {
                lbl_CreateElecResult.Text = "ERROR: " + ex.Message;
            }
        }

        protected void btn_AddPos_Click(object sender, EventArgs e)
        {
            var varUriString = "http://localhost:54103/Service.svc/AddPosition";
            var varHttpWebRequest = (HttpWebRequest)WebRequest.Create(varUriString);
            varHttpWebRequest.ContentType = "application/json";
            varHttpWebRequest.Method = "POST";

            try
            {
                using (var varStreamWriter = new StreamWriter(varHttpWebRequest.GetRequestStream()))
                {

                    StringBuilder sb = new StringBuilder();

                    sb.AppendLine("{");
                    sb.AppendLine("\"Position\":\"" + tb_Pos.Text + "\"");
                    sb.AppendLine(",");
                    sb.AppendLine("\"max_votes\":\"" + tb_MaxV.Text + "\"");
                    sb.AppendLine("}");

                    varStreamWriter.Write(sb.ToString());
                    varStreamWriter.Flush();
                    varStreamWriter.Close();
                }

                var varHttpWebResponse = (HttpWebResponse)varHttpWebRequest.GetResponse();
                using (var varStreamReader = new StreamReader(varHttpWebResponse.GetResponseStream()))
                {
                    var varResponse = varStreamReader.ReadToEnd();
                    lbl_PosResult.Text = varResponse;
                }
            }
            catch (Exception ex)
            {
                lbl_PosResult.Text = "ERROR: " + ex.Message;
            }
        }
    }
}